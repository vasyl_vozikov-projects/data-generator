﻿using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using Infrastructure.Services;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Generator.Repositories
{
    public class GenreRepository : MongoDbRepository<Genre>, IGenreRepository
    {
        public GenreRepository(IMongoDbCollectionProvider provider, IMusicStoreDatabaseSettings settings) : base(provider, "genres", settings)
        {            
        }
        public async Task<Genre> GetByIdAsync(string id)
        {
            return await Collection.Find(g => g.Id == id).FirstOrDefaultAsync();
        }
        public async Task<List<Genre>> GetAllAsync()
        {
            return await Collection.Find(FilterDefinition<Genre>.Empty).ToListAsync();
        }
        public async Task<Genre> CreateOneAsync(Genre genre)
        {            
            await Collection.InsertOneAsync(genre);
            return await GetByIdAsync(genre.Id);
        }
        public async Task<List<Genre>> CreateManyAsync(List<Genre> genres)
        {
            foreach (var gnr in genres)
            {
                if (gnr.Id != null)
                {
                    continue;
                }
                gnr.Id = ObjectId.GenerateNewId().ToString();
            }
            await Collection.InsertManyAsync(genres);
            return await GetNewlyCreatedAsync(genres);
        }
        private async Task<List<Genre>> GetNewlyCreatedAsync(List<Genre> genres)
        {
            var ids = genres.Select(g => g.Id);
            var filterDef = new FilterDefinitionBuilder<Genre>();
            var filter = filterDef.In(g => g.Id, ids);
            return await Collection.Find(filter).ToListAsync();
        }
    }
}
