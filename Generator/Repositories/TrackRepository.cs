﻿using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using Infrastructure.Services;
using MongoDB.Bson;
using MongoDB.Driver;
using static Bogus.DataSets.Name;

namespace Generator.Repositories
{
    public class TrackRepository : MongoDbRepository<Track>, ITrackRepository
    {
        public TrackRepository(IMongoDbCollectionProvider provider, IMusicStoreDatabaseSettings settings) : base(provider, "tracks", settings)
        {            
        }
        public async Task<Track> GetByIdAsync(string id)
        {
            return await Collection.Find(t => t.Id == id).FirstOrDefaultAsync();
        }
        public async Task<List<Track>> GetAllAsync()
        {
            return await Collection.Find(FilterDefinition<Track>.Empty).ToListAsync();
        }
        public async Task<Track> CreateOneAsync(Track track)
        {
            await Collection.InsertOneAsync(track);
            return await GetByIdAsync(track.Id);
        }
        public async Task<List<Track>> CreateManyAsync(List<Track> tracks)
        {
            foreach (var track in tracks)
            {
                if (track.Id != null)
                {
                    continue;
                }
                track.Id = ObjectId.GenerateNewId().ToString();
            }
            await Collection.InsertManyAsync(tracks);
            return await GetNewlyCreatedAsync(tracks);
        }
        private async Task<List<Track>> GetNewlyCreatedAsync(List<Track> tracks)
        {
            var ids = tracks.Select(t => t.Id);
            var filterDef = new FilterDefinitionBuilder<Track>();
            var filter = filterDef.In(t => t.Id, ids);
            return await Collection.Find(filter).ToListAsync();
        }
    }
}
