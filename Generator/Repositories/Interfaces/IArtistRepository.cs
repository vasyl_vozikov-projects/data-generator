﻿using Infrastructure.Models;

namespace Generator.Repositories.Interfaces
{
    public interface IArtistRepository
    {
        Task<Artist> GetByIdAsync(string id);
        Task<List<Artist>> GetAllAsync();
        Task<Artist> CreateOneAsync(Artist artist);
        Task<List<Artist>> CreateManyAsync(List<Artist> artists);
    }
}
