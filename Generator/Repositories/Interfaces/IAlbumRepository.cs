﻿using Infrastructure.Models;

namespace Generator.Repositories.Interfaces
{
    public interface IAlbumRepository
    {
        Task<Album> GetByIdAsync(string id);
        Task<List<Album>> GetAllAsync();
        Task<Album> CreateOneAsync(Album album);
        Task<List<Album>> CreateManyAsync(List<Album> albums);
    }
}
