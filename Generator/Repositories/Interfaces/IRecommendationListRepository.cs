﻿using Infrastructure.Models;

namespace Generator.Repositories.Interfaces
{
    public interface IRecommendationListRepository
    {
        Task<RecommendationList> GetByIdAsync(string id);
        Task<List<RecommendationList>> GetAllAsync();
        Task<RecommendationList> CreateOneAsync(RecommendationList recommendationList);
        Task<List<RecommendationList>> CreateManyAsync(List<RecommendationList> recommendationLists);
    }
}
