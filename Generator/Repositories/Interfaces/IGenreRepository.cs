﻿using Infrastructure.Models;

namespace Generator.Repositories.Interfaces
{
    public interface IGenreRepository
    {
        Task<Genre> GetByIdAsync(string id);
        Task<List<Genre>> GetAllAsync();
        Task<Genre> CreateOneAsync(Genre genre);
        Task<List<Genre>> CreateManyAsync(List<Genre> genres);
    }
}
