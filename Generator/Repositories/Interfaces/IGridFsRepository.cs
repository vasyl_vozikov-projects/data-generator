﻿namespace Generator.Repositories.Interfaces
{
    public interface IGridFsRepository
    {
        Task<string> UploadFileAsync(byte[] bytes);
    }
}
