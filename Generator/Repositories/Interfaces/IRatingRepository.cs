﻿using Infrastructure.Models;

namespace Generator.Repositories.Interfaces
{
    public interface IRatingRepository
    {
        Task<Rating> GetByIdAsync(string id);
        Task<List<Rating>> GetAllAsync();
        Task<Rating> CreateOneAsync(Rating rating);
        Task<List<Rating>> CreateManyAsync(List<Rating> ratings);
    }
}
