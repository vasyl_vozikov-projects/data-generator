﻿using Infrastructure.Models;

namespace Generator.Repositories.Interfaces
{
    public interface ITrackRepository
    {
        Task<Track> GetByIdAsync(string id);
        Task<List<Track>> GetAllAsync();
        Task<Track> CreateOneAsync(Track track);
        Task<List<Track>> CreateManyAsync(List<Track> tracks);
    }
}
