﻿using Infrastructure.Models;

namespace Generator.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<User> GetByIdAsync(string id);
        Task<List<User>> GetAllAsync();
        Task<User> CreateOneAsync(User user);
        Task<List<User>> CreateManyAsync(List<User> users);
    }
}
