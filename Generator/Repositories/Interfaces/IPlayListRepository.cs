﻿using Infrastructure.Models;

namespace Generator.Repositories.Interfaces
{
    public interface IPlayListRepository
    {
        Task<PlayList> GetByIdAsync(string id);
        Task<List<PlayList>> GetAllAsync();
        Task<PlayList> CreateOneAsync(PlayList playList);
        Task<List<PlayList>> CreateManyAsync(List<PlayList> playLists);
    }
}
