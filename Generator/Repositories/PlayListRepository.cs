﻿using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using Infrastructure.Services;
using MongoDB.Bson;
using MongoDB.Driver;
using static Bogus.DataSets.Name;

namespace Generator.Repositories
{
    public class PlayListRepository : MongoDbRepository<PlayList>, IPlayListRepository
    {
        public PlayListRepository(IMongoDbCollectionProvider provider, IMusicStoreDatabaseSettings settings) : base(provider, "playlists", settings)
        {            
        }
        public async Task<PlayList> GetByIdAsync(string id)
        {
            return await Collection.Find(p => p.Id == id).FirstOrDefaultAsync();
        }
        public async Task<List<PlayList>> GetAllAsync()
        {
            return await Collection.Find(FilterDefinition<PlayList>.Empty).ToListAsync();
        }
        public async Task<PlayList> CreateOneAsync(PlayList playList)
        {
            await Collection.InsertOneAsync(playList);
            return await GetByIdAsync(playList.Id);
        }
        public async Task<List<PlayList>> CreateManyAsync(List<PlayList> playLists)
        {
            foreach (var pplayList in playLists)
            {
                if (pplayList.Id != null)
                {
                    continue;
                }
                pplayList.Id = ObjectId.GenerateNewId().ToString();
            }
            await Collection.InsertManyAsync(playLists);
            return await GetNewlyCreatedAsync(playLists);
        }
        private async Task<List<PlayList>> GetNewlyCreatedAsync(List<PlayList> playLists)
        {
            var ids = playLists.Select(p => p.Id);
            var filterDef = new FilterDefinitionBuilder<PlayList>();
            var filter = filterDef.In(p => p.Id, ids);
            return await Collection.Find(filter).ToListAsync();
        }
    }
}