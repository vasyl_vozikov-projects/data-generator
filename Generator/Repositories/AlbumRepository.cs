﻿using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using Infrastructure.Services;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Generator.Repositories
{
    public class AlbumRepository : MongoDbRepository<Album>, IAlbumRepository
    {
        public AlbumRepository(IMongoDbCollectionProvider provider, IMusicStoreDatabaseSettings settings) : base(provider, "albums", settings)
        {            
        }
        public async Task<Album> GetByIdAsync(string id)
        {
            return await Collection.Find(a => a.Id == id).FirstOrDefaultAsync();
        }
        public async Task<List<Album>> GetAllAsync()
        {
            return await Collection.Find(FilterDefinition<Album>.Empty).ToListAsync();
        }
        public async Task<Album> CreateOneAsync(Album album)
        {
            await Collection.InsertOneAsync(album);
            return await GetByIdAsync(album.Id);
        }
        public async Task<List<Album>> CreateManyAsync(List<Album> albums)
        {
            foreach (var album in albums)
            {
                if (album.Id != null)
                {
                    continue;
                }
                album.Id = ObjectId.GenerateNewId().ToString();
            }
            await Collection.InsertManyAsync(albums);
            return await GetNewlyCreatedAsync(albums);
        }
        private async Task<List<Album>> GetNewlyCreatedAsync(List<Album> albums)
        {
            var ids = albums.Select(a => a.Id);
            var filterDef = new FilterDefinitionBuilder<Album>();
            var filter = filterDef.In(a => a.Id, ids);
            return await Collection.Find(filter).ToListAsync();
        }
    }
}