﻿using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using Infrastructure.Services;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Generator.Repositories
{
    public class ArtistRepository : MongoDbRepository<Artist>, IArtistRepository
    {
        public ArtistRepository(IMongoDbCollectionProvider provider, IMusicStoreDatabaseSettings settings) : base(provider, "artists", settings)
        {            
        }
        public async Task<Artist> GetByIdAsync(string id)
        {
            return await Collection.Find(a => a.Id == id).FirstOrDefaultAsync();
        }
        public async Task<List<Artist>> GetAllAsync()
        {
            return await Collection.Find(FilterDefinition<Artist>.Empty).ToListAsync();
        }
        public async Task<Artist> CreateOneAsync(Artist artist)
        {
            await Collection.InsertOneAsync(artist);
            return await GetByIdAsync(artist.Id);
        }
        public async Task<List<Artist>> CreateManyAsync(List<Artist> artists)
        {
            foreach (var artist in artists)
            {
                if (artist.Id != null)
                {
                    continue;
                }
                artist.Id = ObjectId.GenerateNewId().ToString();
            }
            await Collection.InsertManyAsync(artists);
            return await GetNewlyCreatedAsync(artists);
        }        
        private async Task<List<Artist>> GetNewlyCreatedAsync(List<Artist> artists)
        {
            var ids = artists.Select(a => a.Id);
            var filterDef = new FilterDefinitionBuilder<Artist>();
            var filter = filterDef.In(a => a.Id, ids);
            return await Collection.Find(filter).ToListAsync();
        }
    }
}
