﻿using Generator.Repositories.Interfaces;
using Infrastructure.Services;
using MongoDB.Driver.GridFS;
using MongoDB.Driver;
using MongoDB.Bson;

namespace Generator.Repositories
{
    public class GridFsRepository : IGridFsRepository
    {
        private readonly IMusicStoreDatabaseSettings _settings;

        public GridFsRepository(IMusicStoreDatabaseSettings settings)
        {
            _settings = settings;
        }
        public async Task<string> UploadFileAsync(byte[] bytes)
        {
            var client = new MongoClient(_settings.ConnectionString);
            var database = client.GetDatabase(_settings.DatabaseName);
            IGridFSBucket gridFS = new GridFSBucket(database);

            var fileId = ObjectId.GenerateNewId().ToString();
            var id = await gridFS.UploadFromBytesAsync(fileId, bytes);
            return id.ToString();
        }
    }
}
