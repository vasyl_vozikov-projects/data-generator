﻿using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using Infrastructure.Services;
using MongoDB.Bson;
using MongoDB.Driver;
using static Bogus.DataSets.Name;

namespace Generator.Repositories
{
    public class UserRepository : MongoDbRepository<User>, IUserRepository
    {
        public UserRepository(IMongoDbCollectionProvider provider, IMusicStoreDatabaseSettings settings) : base(provider, "users", settings)
        {            
        }
        public async Task<User> GetByIdAsync(string id)
        {
            return await Collection.Find(u => u.Id == id).FirstOrDefaultAsync();
        }
        public async Task<List<User>> GetAllAsync()
        {
            return await Collection.Find(FilterDefinition<User>.Empty).ToListAsync();
        }
        public async Task<User> CreateOneAsync(User user)
        {
            await Collection.InsertOneAsync(user);
            return await GetByIdAsync(user.Id);
        }
        public async Task<List<User>> CreateManyAsync(List<User> users)
        {
            foreach (var user in users)
            {
                if (user.Id != null)
                {
                    continue;
                }
                user.Id = ObjectId.GenerateNewId().ToString();
            }
            await Collection.InsertManyAsync(users);
            return await GetNewlyCreatedAsync(users);
        }
        private async Task<List<User>> GetNewlyCreatedAsync(List<User> users)
        {
            var ids = users.Select(u => u.Id);
            var filterDef = new FilterDefinitionBuilder<User>();
            var filter = filterDef.In(u => u.Id, ids);
            return await Collection.Find(filter).ToListAsync();
        }
    }
}
