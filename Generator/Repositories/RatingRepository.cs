﻿using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using Infrastructure.Services;
using MongoDB.Bson;
using MongoDB.Driver;
using static Bogus.DataSets.Name;

namespace Generator.Repositories
{
    public class RatingRepository : MongoDbRepository<Rating>, IRatingRepository
    {
        public RatingRepository(IMongoDbCollectionProvider provider, IMusicStoreDatabaseSettings settings) : base(provider, "ratings", settings)
        {            
        }
        public async Task<Rating> GetByIdAsync(string id)
        {
            return await Collection.Find(r => r.Id == id).FirstOrDefaultAsync();
        }
        public async Task<List<Rating>> GetAllAsync()
        {
            return await Collection.Find(FilterDefinition<Rating>.Empty).ToListAsync();
        }
        public async Task<Rating> CreateOneAsync(Rating rating)
        {
            await Collection.InsertOneAsync(rating);
            return await GetByIdAsync(rating.Id);
        }
        public async Task<List<Rating>> CreateManyAsync(List<Rating> ratings)
        {
            foreach (var rating in ratings)
            {
                if (rating.Id != null)
                {
                    continue;
                }
                rating.Id = ObjectId.GenerateNewId().ToString();
            }
            await Collection.InsertManyAsync(ratings);
            return await GetNewlyCreatedAsync(ratings);
        }
        private async Task<List<Rating>> GetNewlyCreatedAsync(List<Rating> ratings)
        {
            var ids = ratings.Select(r => r.Id);
            var filterDef = new FilterDefinitionBuilder<Rating>();
            var filter = filterDef.In(r => r.Id, ids);
            return await Collection.Find(filter).ToListAsync();
        }
    }
}
