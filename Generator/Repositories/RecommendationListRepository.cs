﻿using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using Infrastructure.Services;
using MongoDB.Bson;
using MongoDB.Driver;
using static Bogus.DataSets.Name;

namespace Generator.Repositories
{
    public class RecommendationListRepository : MongoDbRepository<RecommendationList>, IRecommendationListRepository
    {
        public RecommendationListRepository(IMongoDbCollectionProvider provider, IMusicStoreDatabaseSettings settings) : base(provider, "recommendationLists", settings)
        {            
        }
        public async Task<RecommendationList> GetByIdAsync(string id)
        {
            return await Collection.Find(r => r.Id == id).FirstOrDefaultAsync();
        }
        public async Task<List<RecommendationList>> GetAllAsync()
        {
            return await Collection.Find(FilterDefinition<RecommendationList>.Empty).ToListAsync();
        }
        public async Task<RecommendationList> CreateOneAsync(RecommendationList recommendationList)
        {
            await Collection.InsertOneAsync(recommendationList);
            return await GetByIdAsync(recommendationList.Id);
        }
        public async Task<List<RecommendationList>> CreateManyAsync(List<RecommendationList> recommendationLists)
        {
            foreach (var rec in recommendationLists)
            {
                if (rec.Id != null)
                {
                    continue;
                }
                rec.Id = ObjectId.GenerateNewId().ToString();
            }
            await Collection.InsertManyAsync(recommendationLists);
            return await GetNewlyCreatedAsync(recommendationLists);
        }
        private async Task<List<RecommendationList>> GetNewlyCreatedAsync(List<RecommendationList> recommendationLists)
        {
            var ids = recommendationLists.Select(r => r.Id);
            var filterDef = new FilterDefinitionBuilder<RecommendationList>();
            var filter = filterDef.In(r => r.Id, ids);
            return await Collection.Find(filter).ToListAsync();
        }
    }
}
