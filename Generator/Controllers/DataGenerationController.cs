﻿using Generator.BLL.Data_Generation_Management;
using Infrastructure.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Generator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataGenerationController : ControllerBase
    {
        private readonly IDataGenerationFactory _factory;
        public DataGenerationController(IDataGenerationFactory factory)
        {
            _factory = factory;
        }
        [HttpPost]
        public async Task<IActionResult> GenerateDataByTypeAsync([FromBody]DataGenerationModel generationModel)
        {
            try
            {
                return Ok(await _factory.ChooseManagerByObjectTypeAsync(generationModel));
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}