﻿using Generator.BLL.Data_Generation_Management;
using Generator.Repositories;
using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using Infrastructure.Services;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Generator
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }
        public Startup(IWebHostEnvironment environment)
        {
            Configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", false, true).Build();
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore().AddNewtonsoftJson();
            services.Configure<MusicStoreDatabaseSettings>(
                            Configuration.GetSection(nameof(MusicStoreDatabaseSettings)));

            services.AddSingleton<IMusicStoreDatabaseSettings>(m =>
                m.GetRequiredService<IOptions<MusicStoreDatabaseSettings>>().Value);

            services.AddSingleton<IMongoClient>(m =>
                new MongoClient(Configuration.GetValue<string>("MusicStoreDatabaseSettings:ConnectionString")));

            services.AddScoped<IMongoDbCollectionProvider, MongoDbCollectionProvider>();
            services.AddScoped<ITaskManager, TaskManager>();
            services.AddScoped<IGridFsRepository, GridFsRepository>();
            services.AddScoped<IDataGenerationFactory, DataGenerationFactory>();

            services.AddScoped<IAlbumRepository, AlbumRepository>();
            services.AddScoped<IArtistRepository, ArtistRepository>();
            services.AddScoped<IGenreRepository, GenreRepository>();
            services.AddScoped<IPlayListRepository, PlayListRepository>();
            services.AddScoped<IRatingRepository, RatingRepository>();
            services.AddScoped<IRecommendationListRepository, RecommendationListRepository>();
            services.AddScoped<ITrackRepository, TrackRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<IDataGenerationManager<Album>, AlbumDataGenerationManager>();
            services.AddScoped<IDataGenerationManager<Artist>, ArtistDataGenerationManager>();
            services.AddScoped<IDataGenerationManager<Genre>, GenreDataGenerationManager>();
            services.AddScoped<IDataGenerationManager<PlayList>, PlayListDataGenerationManager>();
            services.AddScoped<IDataGenerationManager<Rating>, RatingDataGenerationManager>();
            services.AddScoped<IDataGenerationManager<RecommendationList>, RecommendationListDataGenerationManager>();
            services.AddScoped<IDataGenerationManager<Track>, TrackDataGenerationManager>();
            services.AddScoped<IDataGenerationManager<User>, UserDataGenerationManager>();

            services.AddControllers(options => options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true);
        }
        public void Configure(IApplicationBuilder app)
        {
            // Configure the HTTP request pipeline.
            //if (app.Environment.IsDevelopment())
            //{
            //    app.UseSwagger();
            //    app.UseSwaggerUI();
            //}


            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(builder => builder.MapControllers());
        }
    }
}
