﻿using Bogus;
using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using MongoDB.Bson;

namespace Generator.BLL.Data_Generation_Management
{
    public class RatingDataGenerationManager: DataGenerationManager<Rating>, IDataGenerationManager<Rating>
    {
        private readonly IRatingRepository _repository;
        public RatingDataGenerationManager(IRatingRepository repository, IDataGenerationFactory generationFactory, IGridFsRepository gridFs)
            : base(generationFactory, gridFs)
        {
            _repository = repository;
            linkObjects = new List<LinkObject> { new LinkObject { PropertyName = "UserId", LinkedNameSpace = $"{typeof(User).FullName}, Infrastructure"},
                                                 new LinkObject { PropertyName = "RatedItemId", LinkedNameSpace = $"{typeof(Album).FullName}, Infrastructure" },
                                                 new LinkObject { PropertyName = "RatedItemId", LinkedNameSpace = $"{typeof(Track).FullName}, Infrastructure" } };
        }
        public override async Task<List<Rating>> GenerateDataAsync(DataGenerationModel generationModel)
        {
            var rating = new Faker<Rating>()
                .RuleFor(r => r.RatedItemType, f => f.Random.Enum<RatedItemType>())
                .RuleFor(r => r.RatingValue, f => f.Random.Enum<RatingValue>());

            var ratings = rating.Generate(generationModel.Count);
            await GenerateLinkedDataAsync(ratings, linkObjects, generationModel);
            return await _repository.CreateManyAsync(ratings);
        }
    }
}