﻿using Infrastructure.Models;

namespace Generator.BLL.Data_Generation_Management
{
    public interface ITaskManager
    {
        Task<List<T>> RunParallelTasksAsync<T>(DataGenerationModel generationModel, IDataGenerationManager<T> generationManager);
    }
}