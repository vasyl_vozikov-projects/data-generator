﻿using Infrastructure.Models;
using Microsoft.Extensions.DependencyInjection;
namespace Generator.BLL.Data_Generation_Management
{
    public class DataGenerationFactory: IDataGenerationFactory
    {
        private readonly IServiceProvider _provider;
        private readonly ITaskManager _taskManager;

        public DataGenerationFactory(IServiceProvider provider, ITaskManager taskManager)
        {
            _provider = provider;
            _taskManager = taskManager;
        }
        public async Task<object> ChooseManagerByObjectTypeAsync(DataGenerationModel generationModel)
        {
            try
            {
                var type = Type.GetType(generationModel.NameSpace);
                if (type == null)
                {
                    throw new Exception("Generation type not found!");
                }

                switch (type)
                {
                    case Type t when t == typeof(Album):
                        var albumGenManager = _provider.GetService<IDataGenerationManager<Album>>();
                        return await _taskManager.RunParallelTasksAsync(generationModel, albumGenManager);
                    case Type t when t == typeof(Artist):
                        var artistGenManager = _provider.GetService<IDataGenerationManager<Artist>>();
                        return await _taskManager.RunParallelTasksAsync(generationModel, artistGenManager);
                    case Type t when t == typeof(Genre):
                        var genreGenManager = _provider.GetService<IDataGenerationManager<Genre>>();
                        return await _taskManager.RunParallelTasksAsync(generationModel, genreGenManager);
                    case Type t when t == typeof(PlayList):
                        var playListGenManager = _provider.GetService<IDataGenerationManager<PlayList>>();
                        return await _taskManager.RunParallelTasksAsync(generationModel, playListGenManager);
                    case Type t when t == typeof(Rating):
                        var ratingGenManager = _provider.GetService<IDataGenerationManager<Rating>>();
                        return await _taskManager.RunParallelTasksAsync(generationModel, ratingGenManager);
                    case Type t when t == typeof(RecommendationList):
                        var recommendationListGenManager = _provider.GetService<IDataGenerationManager<RecommendationList>>();
                        return await _taskManager.RunParallelTasksAsync(generationModel, recommendationListGenManager);
                    case Type t when t == typeof(Track):
                        var trackGenManager = _provider.GetService<IDataGenerationManager<Track>>();
                        return await _taskManager.RunParallelTasksAsync(generationModel, trackGenManager);
                    case Type t when t == typeof(User):
                        var userGenManager = _provider.GetService<IDataGenerationManager<User>>();
                        return await _taskManager.RunParallelTasksAsync(generationModel, userGenManager);
                }
                return new object();
            }
            catch (Exception ex)
            {
                throw;
            }            
        }
    }
}