﻿using Bogus;
using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using MongoDB.Bson;

namespace Generator.BLL.Data_Generation_Management
{
    public class PlayListDataGenerationManager : DataGenerationManager<PlayList>, IDataGenerationManager<PlayList>
    {
        private readonly IPlayListRepository _repository;
        public PlayListDataGenerationManager(IPlayListRepository repository, IDataGenerationFactory generationFactory, IGridFsRepository gridFs)
            : base(generationFactory, gridFs)
        {
            _repository = repository;
            linkObjects = new List<LinkObject> { new LinkObject { PropertyName = "UserId", LinkedNameSpace = $"{typeof(User).FullName}, Infrastructure" },
                                                 new LinkObject { PropertyName = "TrackIds", LinkedNameSpace = $"{typeof(Track).FullName}, Infrastructure"} };
        }
        public override async Task<List<PlayList>> GenerateDataAsync(DataGenerationModel generationModel)
        {
            var playList = new Faker<PlayList>()
                .RuleFor(p => p.Title, f => f.Random.Word());

            var playLists = playList.Generate(generationModel.Count);
            await GenerateLinkedDataAsync(playLists, linkObjects, generationModel);
            return await _repository.CreateManyAsync(playLists);
        }
    }
}