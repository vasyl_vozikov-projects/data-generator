﻿using Bogus;
using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using MongoDB.Bson;

namespace Generator.BLL.Data_Generation_Management
{
    public class UserDataGenerationManager : DataGenerationManager<User>, IDataGenerationManager<User>
    {
        private readonly IUserRepository _repository;
        public UserDataGenerationManager(IUserRepository repository, IDataGenerationFactory generationFactory, IGridFsRepository gridFs)
            : base(generationFactory, gridFs)
        {
            _repository = repository;
            linkObjects = new List<LinkObject> { new LinkObject { PropertyName = "UserImageUrl", LinkedNameSpace = "$Picture"},
                                                 new LinkObject { PropertyName = "GenreIds", LinkedNameSpace = $"{typeof(Genre).FullName}, Infrastructure" } };
        }
        public override async Task<List<User>> GenerateDataAsync(DataGenerationModel generationModel)
        {
            var user = new Faker<User>()
                .RuleFor(u => u.Name, f => f.Internet.UserName())
                .RuleFor(u => u.Email, f => f.Internet.Email())
                .RuleFor(u => u.Password, f => f.Internet.Password());

            var users = user.Generate(generationModel.Count);
            await GenerateLinkedDataAsync(users, linkObjects, generationModel);
            return await _repository.CreateManyAsync(users);
        }
    }
}