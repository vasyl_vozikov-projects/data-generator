﻿using Infrastructure.Models;

namespace Generator.BLL.Data_Generation_Management
{
    public interface IDataGenerationManager<T>
    {
        Task<List<T>> GenerateDataAsync(DataGenerationModel generationModel);
    }
}
