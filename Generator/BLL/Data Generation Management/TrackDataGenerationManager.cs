﻿using Bogus;
using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using MongoDB.Bson;

namespace Generator.BLL.Data_Generation_Management
{
    public class TrackDataGenerationManager: DataGenerationManager<Track>, IDataGenerationManager<Track>
    {
        private readonly ITrackRepository _repository;
        public TrackDataGenerationManager(ITrackRepository repository, IDataGenerationFactory generationFactory, IGridFsRepository gridFs)
            : base(generationFactory, gridFs)
        {
            _repository = repository;
        }
        public override async Task<List<Track>> GenerateDataAsync(DataGenerationModel generationModel)
        {
            var track = new Faker<Track>()
                .RuleFor(t => t.Title, f => f.Random.Word())
                .RuleFor(t => t.Duration, f => f.Date.Timespan(generationModel.TimeStampRange.Min))
                .RuleFor(t => t.ReleaseDate, f => f.Date.Between(generationModel.DateRange.Min, generationModel.DateRange.Max));

            var tracks = track.Generate(generationModel.Count);
            await GenerateLinkedDataAsync(tracks, linkObjects, generationModel);
            return await _repository.CreateManyAsync(tracks);
        }
    }
}