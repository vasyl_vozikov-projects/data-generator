﻿using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using System.Reflection;

namespace Generator.BLL.Data_Generation_Management
{
    public class DataGenerationManager<T> : IDataGenerationManager<T> where T : class
    {
        protected readonly IDataGenerationFactory _generationFactory;
        protected readonly IGridFsRepository _gridFs;
        protected List<LinkObject> linkObjects = new List<LinkObject>();
        public DataGenerationManager(IDataGenerationFactory generationFactory, IGridFsRepository gridFs)
        {
            _generationFactory = generationFactory;
            _gridFs = gridFs;
        }
        public virtual async Task<List<T>> GenerateDataAsync(DataGenerationModel generationModel)
        {
            List<T> data = new List<T>();
            return data;
        }
        public async Task GenerateLinkedDataAsync(List<T> generationObjects, List<LinkObject> linkObjects, DataGenerationModel generationModel)
        {
            foreach (var generationObject in generationObjects)
            {
                foreach (var linkObject in linkObjects)
                {
                    var linkedProperty = generationObject.GetType().GetProperty(linkObject.PropertyName);                    

                    if (linkObject.LinkedNameSpace == "$Picture")
                    {
                        var generator = new FakeImageGenerator.Generator();
                        var picture = generator.Run(1000000, "Png");
                        var imageId = await _gridFs.UploadFileAsync(picture);

                        SetLinkedProperty(generationObject, linkedProperty, imageId);
                        continue;
                    }
                    var count = GetIdsCountByPropertyType(linkedProperty, generationModel);
                    var result = await _generationFactory.ChooseManagerByObjectTypeAsync(
                        new DataGenerationModel (linkObject.LinkedNameSpace, count, generationModel));
                    var type = Type.GetType(linkObject.LinkedNameSpace);

                    switch (type)
                    {
                        case Type albumType when albumType == typeof(Album):
                            {
                                var albumList = GetCastedList<Album>(result);
                                SetLinkedProperty(generationObject, linkedProperty, albumList);
                                break;
                            }
                        case Type artistType when artistType == typeof(Artist):
                            {
                                var artistList = GetCastedList<Artist>(result);
                                SetLinkedProperty(generationObject, linkedProperty, artistList);
                                break;
                            }
                        case Type genreType when genreType == typeof(Genre):
                            {
                                var genreList = GetCastedList<Genre>(result);
                                SetLinkedProperty(generationObject, linkedProperty, genreList);
                                break;
                            }
                        case Type playListType when playListType == typeof(PlayList):
                            {
                                var playListList = GetCastedList<PlayList>(result);
                                SetLinkedProperty(generationObject, linkedProperty, playListList);
                                break;
                            }
                        case Type ratingType when ratingType == typeof(Rating):
                            {
                                var ratingList = GetCastedList<Rating>(result);
                                SetLinkedProperty(generationObject, linkedProperty, ratingList);
                                break;
                            }
                        case Type recsType when recsType == typeof(RecommendationList):
                            {
                                var recsList = GetCastedList<RecommendationList>(result);
                                SetLinkedProperty(generationObject, linkedProperty, recsList);
                                break;
                            }
                        case Type trackType when trackType == typeof(Track):
                            {
                                var trackList = GetCastedList<Track>(result);
                                SetLinkedProperty(generationObject, linkedProperty, trackList);
                                break;
                            }
                        case Type userType when userType == typeof(User):
                            {
                                var userList = GetCastedList<User>(result);
                                SetLinkedProperty(generationObject, linkedProperty, userList);
                                break;
                            }
                    }
                }
            }
        }
        private List<Y> GetCastedList<Y>(object list)
        {
            return list as List<Y>;
        }
        private int GetIdsCountByPropertyType(PropertyInfo propertyInfo, DataGenerationModel generationModel)
        {
            if (generationModel.LinkedArrayLength == 0)
            {
                return 0;
            }
            if (propertyInfo.PropertyType != typeof(string) && typeof(IEnumerable<string>).IsAssignableFrom(propertyInfo.PropertyType))
            {                
                return generationModel.LinkedArrayLength;
            }
            return 1;
        }
        private void SetLinkedProperty<Y>(T baseObject, PropertyInfo propertyInfo, List<Y> baseModels)
            where Y: class, BaseModel
        {
            if (baseModels == null)
            {
                return;
            }
            if (propertyInfo.PropertyType == typeof(string))
            {
                propertyInfo.SetValue(baseObject, baseModels.FirstOrDefault()?.Id);
            }
            if (propertyInfo.PropertyType == typeof(List<string>))
            {
                propertyInfo.SetValue(baseObject, baseModels.Select(a => a.Id).ToList());
            }
        }
        private void SetLinkedProperty(T baseObject, PropertyInfo propertyInfo, string id)
        {
            propertyInfo.SetValue(baseObject, id);
        }
    }
}