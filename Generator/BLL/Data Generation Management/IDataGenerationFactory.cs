﻿using Infrastructure.Models;

namespace Generator.BLL.Data_Generation_Management
{
    public interface IDataGenerationFactory
    {
        Task<object> ChooseManagerByObjectTypeAsync(DataGenerationModel generationModel);
    }
}
