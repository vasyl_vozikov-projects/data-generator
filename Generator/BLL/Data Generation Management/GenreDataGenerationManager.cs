﻿using Bogus;
using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using MongoDB.Bson;

namespace Generator.BLL.Data_Generation_Management
{
    public class GenreDataGenerationManager : DataGenerationManager<Genre>, IDataGenerationManager<Genre>
    {
        private readonly IGenreRepository _repository;
        public GenreDataGenerationManager(IGenreRepository repository, IDataGenerationFactory generationFactory, IGridFsRepository gridFs)
            : base(generationFactory, gridFs)
        {
            _repository = repository;
        }
        public override async Task<List<Genre>> GenerateDataAsync(DataGenerationModel generationModel)
        {
            var genre = new Faker<Genre>()
                .RuleFor(g => g.Name, f => f.Music.Genre());

            var genres = genre.Generate(generationModel.Count);
            return await _repository.CreateManyAsync(genres);
        }
    }
}