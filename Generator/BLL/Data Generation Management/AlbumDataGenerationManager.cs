﻿using Bogus;
using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using MongoDB.Bson;

namespace Generator.BLL.Data_Generation_Management
{
    public class AlbumDataGenerationManager: DataGenerationManager<Album>, IDataGenerationManager<Album>
    {
        private readonly IAlbumRepository _repository;
        public AlbumDataGenerationManager(IAlbumRepository repository, IDataGenerationFactory generationFactory, IGridFsRepository gridFs)
            : base(generationFactory, gridFs)
        {
            _repository = repository;
            linkObjects = new List<LinkObject> { new LinkObject { PropertyName = "AlbumImageUrl", LinkedNameSpace = "$Picture"},                                                 
                                                 new LinkObject { PropertyName = "GenreId", LinkedNameSpace = $"{typeof(Genre).FullName}, Infrastructure" },                                                 
                                                 new LinkObject { PropertyName = "TrackIds", LinkedNameSpace = $"{typeof(Track).FullName}, Infrastructure" }
                                                 };
        }
        public override async Task<List<Album>> GenerateDataAsync(DataGenerationModel generationModel)
        {
            var album = new Faker<Album>("en")
                .RuleFor(a => a.Title, f => f.Random.Word())
                .RuleFor(a => a.ReleaseDate, f => f.Date.Between(generationModel.DateRange.Min, generationModel.DateRange.Max));

            var albums = album.Generate(generationModel.Count);
            await GenerateLinkedDataAsync(albums, linkObjects, generationModel);
            return await _repository.CreateManyAsync(albums);
        }        
    }
}