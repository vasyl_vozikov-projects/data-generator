﻿using Bogus;
using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using MongoDB.Bson;

namespace Generator.BLL.Data_Generation_Management
{
    public class RecommendationListDataGenerationManager : DataGenerationManager<RecommendationList>, IDataGenerationManager<RecommendationList>
    {
        private readonly IRecommendationListRepository _repository;
        public RecommendationListDataGenerationManager(IRecommendationListRepository repository, IDataGenerationFactory generationFactory, IGridFsRepository gridFs)
            : base(generationFactory, gridFs)
        {
            _repository = repository;
            linkObjects = new List<LinkObject> { new LinkObject { PropertyName = "UserId", LinkedNameSpace = $"{typeof(User).FullName}, Infrastructure" },
                                                 new LinkObject { PropertyName = "GenreId", LinkedNameSpace = $"{typeof(Genre).FullName}, Infrastructure" },
                                                 new LinkObject { PropertyName = "TrackIds", LinkedNameSpace = $"{typeof(Track).FullName}, Infrastructure" } };
        }
        public override async Task<List<RecommendationList>> GenerateDataAsync(DataGenerationModel generationModel)
        {
            var recommendationList = new Faker<RecommendationList>()
                .RuleFor(r => r.Title, f => f.Random.Word());

            var recommendationLists = recommendationList.Generate(generationModel.Count);
            await GenerateLinkedDataAsync(recommendationLists, linkObjects, generationModel);
            return await _repository.CreateManyAsync(recommendationLists);
        }
    }
}