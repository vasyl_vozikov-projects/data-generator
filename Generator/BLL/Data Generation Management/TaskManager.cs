﻿using Infrastructure.Models;

namespace Generator.BLL.Data_Generation_Management
{
    public class TaskManager: ITaskManager
    {
        public TaskManager()
        {
        }
        public async Task<List<T>> RunParallelTasksAsync<T>(DataGenerationModel generationModel, IDataGenerationManager<T> generationManager)
        {
            if (generationModel.Count <= 0)
            {
                return new List<T>();
            }
            if (generationModel.Count <= 100)
            {
                return await generationManager.GenerateDataAsync(generationModel);
            }

            var tasksNumber = generationModel.Count / 100;
            var tasks = new List<Task<List<T>>>(tasksNumber);

            for (int i = 1; i <= tasksNumber; i++)
            {
                generationModel.Count -= (tasksNumber - i) * 100;
                tasks.Add(Task.Run(async () => {
                    var result = await generationManager.GenerateDataAsync(generationModel);
                    return result;
                }));
                generationModel.Count = (tasksNumber - i) * 100;
            }

            var result = await Task.WhenAll(tasks);
            return result.Select(r => r).ToList() as List<T>;
        }
    }
}