﻿using Bogus;
using Generator.Repositories.Interfaces;
using Infrastructure.Models;
using MongoDB.Bson;

namespace Generator.BLL.Data_Generation_Management
{
    public class ArtistDataGenerationManager : DataGenerationManager<Artist>, IDataGenerationManager<Artist>
    {
        private readonly IArtistRepository _repository;
        public ArtistDataGenerationManager(IArtistRepository repository, IDataGenerationFactory generationFactory, IGridFsRepository gridFs)
            : base(generationFactory, gridFs)
        {
            _repository = repository;
            linkObjects = new List<LinkObject> { new LinkObject { PropertyName = "AlbumIds", LinkedNameSpace = $"{typeof(Album).FullName}, Infrastructure" } };
        }
        public override async Task<List<Artist>> GenerateDataAsync(DataGenerationModel generationModel)
        {
            var artist = new Faker<Artist>()
                .RuleFor(a => a.Name, f => f.Random.Word());

            var artists = artist.Generate(generationModel.Count);
            await GenerateLinkedDataAsync(artists, linkObjects, generationModel);
            return await _repository.CreateManyAsync(artists);
        }
    }
}