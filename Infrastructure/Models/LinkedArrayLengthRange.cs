﻿namespace Infrastructure.Models
{
    public class LinkedArrayLengthRange
    {
        public int Min { get; set; }
        public int Max { get; set; }
    }
}
