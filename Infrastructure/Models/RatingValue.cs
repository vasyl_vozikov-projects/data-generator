﻿namespace Infrastructure.Models
{
    public enum RatingValue
    {
        One,
        Two,
        Three,
        Four,
        Five
    }
}
