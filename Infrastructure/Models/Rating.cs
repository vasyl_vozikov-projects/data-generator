﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Infrastructure.Models
{
    public class Rating: BaseModel
    {
        public string Id { get; set; }

        [BsonElement("userId")]
        public string UserId { get; set; }

        [BsonElement("ratedItemId")]
        public string RatedItemId { get; set; }

        [BsonElement("ratedItemType")]
        public RatedItemType RatedItemType { get; set; }

        [BsonElement("ratingValue")]
        public RatingValue RatingValue { get; set; }
    }
}