﻿namespace Infrastructure.Models
{
    public enum RatedItemType
    {
        Album,
        Track
    }
}
