﻿namespace Infrastructure.Models
{
    public interface BaseModel
    {
        public string Id { get; set; }
    }
}
