﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Infrastructure.Models
{
    public class RecommendationList: BaseModel
    {
        public string Id { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("userId")]
        public string UserId { get; set; }

        [BsonElement("genreId")]
        public string GenreId { get; set; }

        [BsonElement("trackIds")]
        public List<string> TrackIds { get; set; }
    }
}
