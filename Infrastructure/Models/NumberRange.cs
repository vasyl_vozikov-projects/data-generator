﻿namespace Infrastructure.Models
{
    public class NumberRange
    {
        public int Min { get; set; } = 5;
        public int Max { get; set; } = 20;
    }
}