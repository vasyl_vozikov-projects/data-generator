﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Infrastructure.Models
{
    public class Track: BaseModel
    {
        public string Id { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("duration")]
        public TimeSpan Duration { get; set; }

        [BsonElement("releaseDate")]
        public DateTime ReleaseDate { get; set; }   
    }    
}