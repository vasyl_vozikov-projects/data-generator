﻿namespace Infrastructure.Models
{
    public class LinkObject
    {
        public string PropertyName { get; set; }
        public string LinkedNameSpace { get; set; }
    }
}
