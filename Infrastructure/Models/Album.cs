﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Infrastructure.Models
{
    public class Album: BaseModel
    {
        public string Id { get; set; }

        [BsonElement("imageUrl")]
        public string AlbumImageUrl { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("genreId")]
        public string GenreId { get; set; }

        [BsonElement("trackIds")]
        public List<string> TrackIds { get; set; }

        [BsonElement("releaseDate")]
        public DateTime ReleaseDate { get; set; }
    }
}