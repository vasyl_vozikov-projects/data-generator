﻿namespace Infrastructure.Models
{
    public class DataGenerationModel
    {
        public string NameSpace { get; set; }
        public int Count { get; set; } = 100;
        public NumberRange NumberRange { get; set; }
        public DateRange DateRange { get; set; }
        public TimeStampRange TimeStampRange { get; set; }
        public StringLengthRange StringLengthRange { get; set; }
        public int LinkedArrayLength { get; set; } = 3;

        public DataGenerationModel()
        {

        }
        public DataGenerationModel(string linkedNameSpace, int count, DataGenerationModel generationModel)
        {
            NameSpace = linkedNameSpace;
            Count = count;
            NumberRange = generationModel.NumberRange;
            DateRange = generationModel.DateRange;
            TimeStampRange = generationModel.TimeStampRange;
            StringLengthRange = generationModel.StringLengthRange;
            LinkedArrayLength = generationModel.LinkedArrayLength;
        }
    }
}