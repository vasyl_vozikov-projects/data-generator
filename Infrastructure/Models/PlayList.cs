﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Infrastructure.Models
{
    public class PlayList: BaseModel
    {
        public string Id { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("userId")]
        public string UserId { get; set; }

        [BsonElement("trackIds")]
        public List<string> TrackIds { get; set; }
    }
}