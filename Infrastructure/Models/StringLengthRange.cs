﻿namespace Infrastructure.Models
{
    public class StringLengthRange
    {
        public int Min { get; set; } = 1;
        public int Max { get; set; } = 50;
    }
}
