﻿namespace Infrastructure.Models
{
    public class TimeStampRange
    {
        public TimeSpan Min { get; set; } = new TimeSpan(0, 0, 1, 0, 0);
        public TimeSpan Max { get; set; } = new TimeSpan(0, 2, 0, 0, 0);
    }
}
