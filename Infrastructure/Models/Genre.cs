﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Infrastructure.Models
{
    public class Genre: BaseModel
    {
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }
    }
}
