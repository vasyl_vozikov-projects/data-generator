﻿namespace Infrastructure.Models
{
    public class DateRange
    {
        public DateTime Min { get; set; } = DateTime.UnixEpoch;
        public DateTime Max { get; set; } = DateTime.Today;
    }
}
