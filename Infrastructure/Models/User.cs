﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Infrastructure.Models
{
    public class User: BaseModel
    {
        public string Id { get; set; }

        [BsonElement("imageUrl")]
        public string UserImageUrl { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("genreIds")]
        public List<string> GenreIds { get; set; }
    }
}
