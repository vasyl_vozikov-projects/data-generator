﻿using MongoDB.Driver;

namespace Infrastructure.Services
{
    public interface IMongoDbCollectionProvider
    {
        IMongoCollection<T> GetCollection<T>(string database, string collection);
    }
}