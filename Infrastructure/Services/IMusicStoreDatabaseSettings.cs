﻿namespace Infrastructure.Services
{
    public interface IMusicStoreDatabaseSettings
    {
        string DatabaseName { get; set; }
        string ConnectionString { get; set; }
    }
}
