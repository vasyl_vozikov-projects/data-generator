﻿using MongoDB.Driver;

namespace Infrastructure.Services
{
    public class MongoDbCollectionProvider: IMongoDbCollectionProvider
    {
        private readonly IMongoClient _client;

        public MongoDbCollectionProvider(IMongoClient client)
        {
            _client = client;
        }

        public IMongoCollection<T> GetCollection<T>(string database, string collection)
        {
            var db = _client.GetDatabase(database);
            return db.GetCollection<T>(collection);
        }
    }
}
