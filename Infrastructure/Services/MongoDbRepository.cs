﻿using MongoDB.Driver;

namespace Infrastructure.Services
{
    public class MongoDbRepository<T> : IMongoDbRepository where T : class
    {
        protected IMongoCollection<T> Collection { get; }

        public MongoDbRepository(IMongoDbCollectionProvider provider, string collectionName, IMusicStoreDatabaseSettings settings)
        {
            Collection ??= provider.GetCollection<T>(settings.DatabaseName, collectionName);
        }
    }
}
