﻿namespace Infrastructure.Services
{
    public class MusicStoreDatabaseSettings : IMusicStoreDatabaseSettings
    {
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
    }
}